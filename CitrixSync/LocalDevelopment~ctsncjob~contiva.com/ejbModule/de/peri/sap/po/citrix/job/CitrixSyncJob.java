package de.peri.sap.po.citrix.job;

import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import com.sap.scheduler.runtime.JobContext;
import com.sap.scheduler.runtime.JobParameter;
import com.sap.scheduler.runtime.mdb.MDBJobImplementation;
//Specify message selector 
//and destination type

import de.peri.sap.po.ejb.citrix.sync.CitrixSync;

@SuppressWarnings("serial")
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "JobDefinition='CitrixSyncJob'"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue") })

public class CitrixSyncJob extends MDBJobImplementation {

	public void onJob(JobContext ctx) {

		// Implement business logic
		Logger log = ctx.getLogger();

		String iseHost = "";
		int isePort = 0;
		String iseUser = "";
		String isePwd = "";
		String mdmHost = "";
		int mdmPort = 0;
		String mdmUser = "";
		String mdmPwd = "";

		JobParameter iseHostParameter = ctx.getJobParameter("ISE_HOST");
		if (iseHostParameter != null) {
			iseHost = iseHostParameter.getStringValue();
		} else {
			log.severe("Ise Host not set, please configure Job");
		}
		
		JobParameter isePortParameter = ctx.getJobParameter("ISE_PORT");
		if (isePortParameter != null) {
			isePort = isePortParameter.getIntegerValue();
		} else {
			log.severe("Ise Port not set, please configure Job");
		}
		JobParameter iseUserParameter = ctx.getJobParameter("ISE_USERNAME");
		if (iseUserParameter != null) {
			iseHost = iseUserParameter.getStringValue();
		} else {
			log.severe("Ise Username not set, please configure Job");
		}
		JobParameter isePwdParameter = ctx.getJobParameter("ISE_PWD");
		if (isePwdParameter != null) {
			iseHost = isePwdParameter.getStringValue();
		} else {
			log.severe("Ise Passwort not set, please configure Job");
		}
		
		JobParameter mdmHostParameter = ctx.getJobParameter("MDM_HOST");
		if (mdmHostParameter != null) {
			mdmHost = mdmHostParameter.getStringValue();
		} else {
			log.severe("mdm Host not set, please configure Job");
		}
		
		JobParameter mdmPortParameter = ctx.getJobParameter("MDM_PORT");
		if (mdmPortParameter != null) {
			mdmPort = mdmPortParameter.getIntegerValue();
		} else {
			log.severe("mdm Port not set, please configure Job");
		}
		JobParameter mdmUserParameter = ctx.getJobParameter("MDM_USERNAME");
		if (mdmUserParameter != null) {
			mdmHost = mdmUserParameter.getStringValue();
		} else {
			log.severe("mdm Username not set, please configure Job");
		}
		JobParameter mdmPwdParameter = ctx.getJobParameter("MDM_PWD");
		if (mdmPwdParameter != null) {
			mdmHost = mdmPwdParameter.getStringValue();
		} else {
			log.severe("mdm Passwort not set, please configure Job");
		}
		
		try{
			CitrixSync citSync = new CitrixSync(iseHost, isePort, iseUser, isePwd, mdmHost, mdmPort, mdmUser, mdmPwd);
			citSync.processSync();
		}
		catch (Exception e){
			log.severe(e.getMessage());
		}
	}
}
