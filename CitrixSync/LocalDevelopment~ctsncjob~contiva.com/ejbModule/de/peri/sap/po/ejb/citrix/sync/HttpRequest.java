package de.peri.sap.po.ejb.citrix.sync;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import com.sap.tc.logging.Location;

public class HttpRequest {
	
	private StringBuilder Log;
	


	private static Location location;
	
	public HttpRequest(StringBuilder log){
		this.Log = log;
	}

	public InputStream getHttpResponse(String urlString, String Accept, HashMap<String, String> headers, String method,
			String user, String pwd) throws IOException {

		URL url;
		url = new URL(urlString);

		HttpURLConnection con = (HttpURLConnection) url.openConnection();

		con.setRequestMethod(method);
		con.setRequestProperty("Content-Type", "application/xml");
		// con.setRequestProperty("Accept", Accept);

		for (Map.Entry<String, String> entry : headers.entrySet()) {
			con.setRequestProperty(entry.getKey(), entry.getValue());
		}

		con.setDoOutput(true);

		myAuthenticator.setDefault(new myAuthenticator(user, pwd));

		return con.getInputStream();
	}

	public InputStream getHttpsResponse(String urlString, String Accept, HashMap<String, String> headers, String method,
			String user, String pwd)
			throws IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {

		URL url;
		url = new URL(urlString);

		String keyStoreType = KeyStore.getDefaultType();
		KeyStore keyStore = KeyStore.getInstance(keyStoreType);

		String algorithm = TrustManagerFactory.getDefaultAlgorithm();
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(algorithm);
		tmf.init(keyStore);

		SSLContext context = SSLContext.getInstance("TLS");
		context.init(null, tmf.getTrustManagers(), null);

		// Open a HTTP connection to the URL
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
		con.setSSLSocketFactory(context.getSocketFactory());

		con.setRequestMethod(method);
		con.setRequestProperty("Content-Type", "application/xml");

		for (Map.Entry<String, String> entry : headers.entrySet()) {
			con.setRequestProperty(entry.getKey(), entry.getValue());
		}

		con.setDoInput(true);
		myAuthenticator.setDefault(new myAuthenticator(user, pwd));

		InputStream ret = con.getInputStream();

		return ret;

	}

	public InputStream getHttpsResponseWithNoValidation(String urlString, HashMap<String, String> headers,
			String method, String user, String pwd) throws MalformedURLException  {

		URL url = new URL(urlString);
		
		
		InputStream is = null;

		// this part is necessary to accept self signed certificates
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
			}

		} };

		SSLContext sc = null;
		try {
			sc = SSLContext.getInstance("SSL");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		try {
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier
		HostnameVerifier validHosts = new HostnameVerifier() {

			@Override
			public boolean verify(String arg0, SSLSession arg1) {
				return true;
			}

		};

		HttpsURLConnection.setDefaultHostnameVerifier(validHosts);

		// set necessary header fields
		try {
			// initiate connection but do not open it yet
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			con.setRequestMethod(method);
			for (Map.Entry<String, String> entry : headers.entrySet()) {
				con.setRequestProperty(entry.getKey(), entry.getValue());
			}

			// set authentication
			myAuthenticator.setDefault(new myAuthenticator(user, pwd));

			is = con.getInputStream();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();

		}

		return is;

	}

	public int getHttpsResponseFromPostWithNoValidation(String urlString, HashMap<String, String> headers,
			String method, String user, String pwd, String content) throws UnsupportedEncodingException, IOException {

		URL url = new URL(urlString);

		// this part is necessary to accept self signed certificates
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
			}

		} };

		SSLContext sc = null;
		try {
			sc = SSLContext.getInstance("SSL");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		try {
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier
		HostnameVerifier validHosts = new HostnameVerifier() {

			@Override
			public boolean verify(String arg0, SSLSession arg1) {
				return true;
			}

		};

		HttpsURLConnection.setDefaultHostnameVerifier(validHosts);

		// initiate connection but do not open it yet
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

		// set necessary header fields
		con.setRequestMethod(method);

		for (Map.Entry<String, String> entry : headers.entrySet()) {
			con.setRequestProperty(entry.getKey(), entry.getValue());
		}

		con.setRequestMethod(method);
		con.setRequestProperty("Content-Length", String.valueOf(content.getBytes("UTF-8").length));
		con.setDoOutput(true);

		con.getOutputStream().write(content.getBytes("UTF-8"));

		// set authentication
		myAuthenticator.setDefault(new myAuthenticator(user, pwd));

		int responseCode = 0;

		try {
			// get response code and payload
			InputStream is = con.getInputStream();
			responseCode = con.getResponseCode();

			byte[] response = new byte[10000];

			while (is.read() > 0) {
				is.read(response);
				location.debugT(new String(response));
				System.out.println(new String(response));
				Log.append(System.lineSeparator() + new String(response));
			}

		} catch (IOException e) {

			// something went wrong, get response code and error payload
			InputStream es = con.getErrorStream();

			byte[] reading = new byte[10000];

			while (es.read() > 0) {
				responseCode = con.getResponseCode();
				es.read(reading);
//				location.debugT(new String(reading));
				System.out.println(new String(reading));
				Log.append(System.lineSeparator() + new String(reading));
			}
		}

		return responseCode;

	}

}
