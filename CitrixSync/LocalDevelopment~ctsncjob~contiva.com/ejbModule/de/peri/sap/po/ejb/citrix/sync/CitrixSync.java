package de.peri.sap.po.ejb.citrix.sync;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

import org.xml.sax.SAXException;

import com.sap.tc.logging.Location;

public class CitrixSync {

	private static Location location;
	MdMDevice mdmDevice;
	IseDevice iseDevice;

	String Log = "";
	
	/**
	 * Default constructor.
	 */
	@SuppressWarnings("unused")
	private CitrixSync() {
	}

	public CitrixSync(String iseHost, int isePort, String iseUser, String isePwd, String mdmHost, int mdmPort,
			String mdmUser, String mdmPwd)
			throws IOException, ParserConfigurationException, SAXException, JAXBException, XMLStreamException {

		location = Location.getLocation("de.peri.sap.po.ejb.citrix.sync");
		location.entering("CitrixSync()");

		// create MDMDevice List and populate it via http call
		mdmDevice = new MdMDevice(mdmHost, mdmPort, mdmUser, mdmPwd);
		// create ISEDevice List and populate it via rest api call
		iseDevice = new IseDevice(iseHost, isePort, iseUser, isePwd);
		processSync();
	}

	public void processSync() throws IOException {

		// complete business logic is implemented in here

		// add or delete from iseDevice List
		compareAndProcessDevices(mdmDevice, iseDevice);

	}

	private void compareAndProcessDevices(MdMDevice mdmDevice, IseDevice iseDevice) throws IOException {

		// comparison between the different device lists is implemented here
		boolean found = false;
		
		String mdmMac = "";
		String iseMac = "";
		
		for(int k=0; k < mdmDevice.devices.size(); k++) {
			mdmMac = mdmMac + System.lineSeparator() + mdmDevice.devices.get(k).getMacAddress();
			printToFile(mdmMac, "MDM Devices.log");
		}
		
		for(int h=0; h < iseDevice.devices.size(); h++) {
			iseMac = iseMac + System.lineSeparator() + iseDevice.devices.get(h).getMacAddress();
			printToFile(iseMac, "ISE Devices.log");
		}

		// loop through MdMDevice List and check if MdmDevice MAC is already
		// contained in IseDevice List
		for (int i = 0; i < mdmDevice.devices.size(); i++) {
			for (int j = 0; j < iseDevice.devices.size(); j++) {
				if (mdmDevice.devices.get(i).getMacAddress()
						.equals(iseDevice.devices.get(j).getMacAddress().replaceAll(":", ""))) {
					found = true;
					iseDevice.devices.get(j).setProcessed(true);
					System.out.println("Device with MacAddress" + mdmDevice.devices.get(i).getMacAddress() + " found.");
					Log = Log + System.lineSeparator() + "Device with MacAddress" + mdmDevice.devices.get(i).getMacAddress() + " found.";
					
				}
			}
			if (found != true) {
				// Mdm not found in ISE List --> create new Ise Endpoint
				System.out.println(
						"**** Device with MacAddress" + mdmDevice.devices.get(i).getMacAddress() + " not found.");
				Log = Log + System.lineSeparator() + "**** Device with MacAddress" + mdmDevice.devices.get(i).getMacAddress() + " not found.";
				location.infoT(
						"Added Device with Mac: " + mdmDevice.devices.get(i).getMacAddress() + " to ISE system.");

				iseDevice.addIseDevice(mdmDevice.devices.get(i));
			}
			found = false;
		}

		// all iseDevices which are not processed are no longer valid and will
		// be deleted
		for (int j = 0; j < iseDevice.devices.size(); j++) {
			if (!iseDevice.devices.get(j).isProcessed()) {
				iseDevice.deleteIseDevice(iseDevice.devices.get(j));
				location.infoT(
						"Deleted Device with Mac: " + iseDevice.devices.get(j).getMacAddress() + " from ISE system.");
				System.out.println(
						"Deleted Device with Mac: " + iseDevice.devices.get(j).getMacAddress() + " from ISE system.");
				Log = Log + System.lineSeparator() + "Deleted Device with Mac: " + iseDevice.devices.get(j).getMacAddress() + " from ISE system.";
			}
		}
		printToFile(Log, "Sync.Log");

	}
	
	private void printToFile(String text, String fileName)
	{
		try (PrintStream out = new PrintStream(new FileOutputStream(fileName))){
			out.append(text);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		try {
			@SuppressWarnings("unused")
			CitrixSync cs = new CitrixSync(args[0], Integer.parseInt(args[1]), args[2], args[3], args[4],
					Integer.parseInt(args[5]), args[6], args[7]);
			// System.out.println(cs.testIseConnection(cs.iseDevice));
		} catch (Exception e) {

			location.errorT(e.getMessage());
			e.printStackTrace();
		}

	}

}
