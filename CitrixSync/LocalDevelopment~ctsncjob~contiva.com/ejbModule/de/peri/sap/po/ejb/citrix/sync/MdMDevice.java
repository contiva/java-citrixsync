package de.peri.sap.po.ejb.citrix.sync;

import com.air_watch.servicemodel.resources.DeviceSearchResult;
import com.sap.tc.logging.Location;

import java.io.IOException;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.xml.sax.SAXException;

public class MdMDevice {

	int MdmPage = 0;
	int MdmTotal = 9999;
	private static Location location;
	
	StringBuilder log = new StringBuilder();

	private static final int PAGESIZE = 500;

	public List<DeviceSearchResult.Devices> devices = null;
	HttpRequest httpReq = new HttpRequest(log);

	String host;
	int port;
	String user;
	String pwd;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	@SuppressWarnings("unused")
	private MdMDevice() {

	}

	public MdMDevice(String host, int port, String user, String pwd) throws IOException, ParserConfigurationException, SAXException, JAXBException, XMLStreamException {
		
		location = Location.getLocation("de.peri.sap.po.ejb.citrix.sync.MDMDevice");
		location.entering("MdMDevice()");
		
		setHost(host);
		setPort(port);
		setUser(user);
		setPwd(pwd);

		this.devices = getDevicesFromMDM();
	}

	public List<DeviceSearchResult.Devices> getDevicesFromMDM()
			throws IOException, ParserConfigurationException, SAXException, JAXBException, XMLStreamException {

		List<DeviceSearchResult.Devices> devices = new ArrayList<DeviceSearchResult.Devices>();

		while ((MdmPage + 1) * PAGESIZE < MdmTotal) {

			String uri = host + "?page=" + MdmPage + "&pagesize=" + PAGESIZE;
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put("aw-tenant-code", "P+CGQwPpfF7xLX0OjkFkmnz/I3ViJ5NN43CiLErFBsU=");

			// perform http call
			InputStream is = httpReq.getHttpResponse(uri, "application/xml", headers, "GET", user,
					pwd);

			parseMdmPage(devices, is);
			location.infoT("Page " + MdmPage + " loaded.");

			MdmPage += 1;
			System.out.println(MdmPage);

		}
		location.infoT("Total of " + MdmTotal + " devices loaded.");
		return devices;
	}
	

	private void parseMdmPage(List<DeviceSearchResult.Devices> devices, InputStream is)
			throws XMLStreamException, JAXBException {

		boolean isTotal = false;
		boolean isDevice = false;

		// parse xml page with Stax Parser to unmarschall devices XML to Java
		// object
		
		
		XMLStreamReader reader = null;
		// read from stream into reader
		reader = XMLInputFactory.newInstance().createXMLStreamReader(is);
		while (reader.hasNext()) {

			switch (reader.getEventType()) {
			case XMLStreamConstants.START_ELEMENT:

				if (reader.getLocalName().equals("Total")) {
					isTotal = true;
				}

				if (reader.getLocalName().equals("DeviceSearchResult")) {

					JAXBContext jaxbContext = JAXBContext.newInstance(DeviceSearchResult.class);
					Unmarshaller jaxbUnMarschaller = jaxbContext.createUnmarshaller();

					isDevice = true;
					DeviceSearchResult dev = (DeviceSearchResult) jaxbUnMarschaller.unmarshal(reader);
					MdmTotal = dev.getTotal();
					for (int i = 0; i < dev.getDevices().size(); i++) {
						devices.add(dev.getDevices().get(i));
						System.out.println(dev.getDevices().get(i).getId());
					}
				}
				break;

			case XMLStreamConstants.END_ELEMENT:
				break;
			case XMLStreamConstants.END_DOCUMENT:
				reader.close();
				break;

			case XMLStreamConstants.CHARACTERS:

				if (isTotal) {
					MdmTotal = Integer.parseInt(reader.getText());
					System.out.println(MdmTotal);
					isTotal = false;
				}

				break;
			}
			if (!isDevice) {
				// Unmarshalling automatically sets the cursor to the next
				// devices element
				// after creating the java object --> skip
				reader.next();
			} else {
				isDevice = false;
			}
		}
	}

}
