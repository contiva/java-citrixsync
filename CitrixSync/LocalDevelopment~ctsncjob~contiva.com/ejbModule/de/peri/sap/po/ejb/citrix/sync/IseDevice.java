package de.peri.sap.po.ejb.citrix.sync;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import de.peri.sap.po.ejb.citrix.sync.JAXBHelper;

import com.air_watch.servicemodel.resources.DeviceSearchResult;
import com.cisco.ise.ers.identity.Endpoint;
import com.cisco.ise.ers.identity.ObjectFactory;

public class IseDevice {

	private static final String GROUPNAME = "GuestEndpoints_static";


	private HashMap<String, String> headers = new HashMap<String, String>();
	public List<IseDevices> devices = null;

	private String groupIdUri = "ers/config/endpointgroup?filter=name.EQ.";
	private String operateEndpoint = "ers/config/endpoint/";

	private String getUri = "https://%s:%d/ers/config/endpoint?size=%d&page=%d&filter=groupId.EQ.%s";

	// init to 1, to get number of devices on first http call
	private int Total = 1;
	private int page = 1;
	private int size = 100;

	private String localhost;
	private String groupID;

	private String User;
	private String Password;
	private int port = 9060;

	private StringBuilder Log = new StringBuilder();
	private HttpRequest httpReq = new HttpRequest(Log);

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getLocalhost() {
		return localhost;
	}

	public void setLocalhost(String localhost) {
		this.localhost = localhost;
	}

	public String getUser() {
		return User;
	}

	public void setUser(String user) {
		User = user;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getGroupID() {
		return groupID;
	}

	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}

	@SuppressWarnings("unused")
	private IseDevice() {
		// private default constructor to prevent from initializing an empty
		// IseDevice
	}

	public IseDevice(String host, int port, String user, String pwd)
			throws IOException, ParserConfigurationException, SAXException {

		devices = new ArrayList<IseDevices>();

		setLocalhost(host);
		setPort(port);
		setUser(user);
		setPassword(pwd);

		setHeaders();

		setGroupID(getIdForGroupFromIse(GROUPNAME));
		this.devices = getAllDevicesFromIse();
	}

	private String getIdForGroupFromIse(String group) throws IOException, ParserConfigurationException, SAXException {

		String uri = "https://" + localhost + ":" + port + "/" + groupIdUri + group;

		System.out.println(uri);

		InputStream is;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;

		is = httpReq.getHttpsResponseWithNoValidation(uri, headers, "GET", getUser(), getPassword());

		db = dbf.newDocumentBuilder();
		Document doc;
		doc = db.parse(is);

		Node resource = doc.getFirstChild().getChildNodes().item(0).getChildNodes().item(0);

		if (resource.hasAttributes()) {
			NamedNodeMap attributes = resource.getAttributes();
			groupID = attributes.getNamedItem("id").getNodeValue();
		}

		return groupID;
	}

	public List<IseDevices> getAllDevicesFromIse() throws IOException, ParserConfigurationException, SAXException {

		// init to first page
		String url = String.format(getUri, localhost, port, size, page, groupID);
		overwriteAcceptForGet();

		// System.out.println(url);

		InputStream is;

		// first http call retrieves number of devices
		is = httpReq.getHttpsResponseWithNoValidation(url, headers, "GET", getUser(), getPassword());

		// get complete number of devices
		Total = parseIsePageForNumber(is);
		if (Total > 0) {

			int numberOfPages = 0;

			// is the chunk size (default 100) bigger than total, only get one
			// chunk
			if (Total < size) {
				size = Total;
			}

			if (Total % size > 0) {
				numberOfPages = Total / size + 1;
			} else {
				numberOfPages = Total / size;
			}

			while (page <= numberOfPages) {
				if ((Total - (page * size) > 0 && (Total - (page * size) < size))) {
					size = Total - (page * size);
				}
				url = String.format(getUri, localhost, port, size, page, groupID);
				System.out.println(url);

				// get all devices
				is = httpReq.getHttpsResponseWithNoValidation(url, headers, "GET", getUser(), getPassword());

				parseIsePage(is);

				page += 1;
			}
		}

		return devices;
	}

	public void deleteIseDevice(IseDevices device) throws IOException {

		String url = "https://" + localhost + ":" + port + "/" + operateEndpoint + device.getId();
		overwriteAcceptForDelete();

		httpReq.getHttpsResponseWithNoValidation(url, headers, "DELETE", getUser(), getPassword());
	}

	public void deleteIseDeviceOverPO(IseDevice device) {
		// TO DO
	}

	public void addIseDevice(DeviceSearchResult.Devices device) {

		String url = "https://" + localhost + ":" + port + "/" + operateEndpoint;
		System.out.println(url);
		Log.append(System.lineSeparator() + url);
		overwriteAcceptForAdd();

		try {
			String content = createHttpPostContent(device);
			int respCode = httpReq.getHttpsResponseFromPostWithNoValidation(url, headers, "POST", getUser(),
					getPassword(), content);
			

			System.out.println(respCode);
			Log.append(System.lineSeparator() + respCode);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JAXBException e1) {
			e1.printStackTrace();
		}

		printToFile(Log.toString(), "IseSync.log");

	}

	public String createHttpPostContent(DeviceSearchResult.Devices device) throws JAXBException {

		com.cisco.ise.ers.identity.ObjectFactory identityFactory = new ObjectFactory();
		JAXBHelper jaxbh = new JAXBHelper(identityFactory.getClass().getPackage().getName());

		Endpoint endpoint = new Endpoint();

		endpoint.setName(device.getMacAddress());
		endpoint.setMac(device.getMacAddress());
		endpoint.setPortalUser(device.getUserName() + "@corp.peri");
		endpoint.setDescription("Automatically created through AirWatch Sync:" + device.getDeviceFriendlyName());
		endpoint.setGroupId(groupID);
		endpoint.setStaticGroupAssignment(true);
		endpoint.setStaticProfileAssignment(false);

		JAXBElement<Endpoint> jaxbEndPoint = identityFactory.createEndpoint(endpoint);

		String endpointAsXml = jaxbh.marshal(jaxbEndPoint);

		System.out.println(endpointAsXml);
		Log.append(Log + System.lineSeparator() + endpointAsXml);
		return endpointAsXml;
	}

	private int parseIsePageForNumber(InputStream is) throws ParserConfigurationException, SAXException, IOException {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;

		db = dbf.newDocumentBuilder();
		Document doc;
		doc = db.parse(is);

		Node rootNode = doc.getFirstChild();

		if (rootNode.hasAttributes()) {
			NamedNodeMap attributes = rootNode.getAttributes();
			for (int i = 0; i < attributes.getLength(); i++) {

				if (attributes.item(i).getNodeName().equals("total")) {
					Total = Integer.parseInt(attributes.item(i).getTextContent());
				}
			}
		}

		return Total;

	}

	private void parseIsePage(InputStream is) throws ParserConfigurationException, SAXException, IOException {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db;

		db = dbf.newDocumentBuilder();
		Document doc;
		doc = db.parse(is);

		NodeList nodes = doc.getElementsByTagNameNS("*", "resource");

		for (int i = 0; i < nodes.getLength(); i++) {
			IseDevices newDevice = new IseDevices();
			if (nodes.item(i).hasAttributes()) {
				NamedNodeMap attributes = nodes.item(i).getAttributes();
				for (int j = 0; j < attributes.getLength(); j++) {
					if (attributes.item(j).getNodeName().equals("name")) {
						newDevice.setMacAddress(attributes.item(j).getTextContent());
					}
					if (attributes.item(j).getNodeName().equals("id")) {
						newDevice.setId(attributes.item(j).getTextContent());
					}
				}
				devices.add(newDevice);
			}
		}

	}

	private void printToFile(String text, String fileName) {
		try (PrintStream out = new PrintStream(new FileOutputStream(fileName))) {
			out.append(text);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setHeaders() {

		headers.put("Accept", "application/vnd.com.cisco.ise.identity.endpointgroup.1.0+xml");
		headers.put("Accept-Search-Result", "application/vnd.com.cisco.ise.ers.searchresult.2.0+xml");
		headers.put("Encoding", "UTF-8");
		headers.put("Content-Type", "application/vnd.com.cisco.ise.ers.ersresponse.1.1+xml;charset=utf-8");

	}

	private void overwriteAcceptForGet() {
		headers.put("Accept", "application/vnd.com.cisco.ise.identity.endpoint.1.0+xml");
	}

	private void overwriteAcceptForDelete() {
		headers.put("Accept", "application/vnd.com.cisco.ise.identity.endpoint.1.1+xml");
	}

	private void overwriteAcceptForAdd() {
		headers.put("Content-Type", "application/vnd.com.cisco.ise.identity.endpoint.1.1+xml; charset=utf-8");
		headers.put("Accept", "application/vnd.com.cisco.ise.identity.endpoint.1.1+xml");
	}

}
